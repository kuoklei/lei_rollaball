﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class enemyMovement : MonoBehaviour
{
    NavMeshAgent navMeshAgent;
    public float timerForNewPath;
    public GameObject pickUp;
    public Text enemyText;
    bool inCoRoutine;
    int enemyCount;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

    }

    void Update()
    {
        if (!inCoRoutine)
            StartCoroutine(DoSomething());
    }

    // Change the direction
    Vector3 getNewRandomPosition()
    {
        float x = Random.Range(-20, 20);
        float z = Random.Range(-20, 20);

        Vector3 pos = new Vector3(x, 0, z);
        return pos;
    }

    // Wait
    IEnumerator DoSomething()
    {
        inCoRoutine = true;
        yield return new WaitForSeconds(timerForNewPath);
        GetNewPath();
        inCoRoutine = false;
    }

    // go to the new direction
    void GetNewPath()
    {
        navMeshAgent.SetDestination(getNewRandomPosition());
    }

    // Update is called once per frame
    // I want to make it can destory pickUp object
    // I tried couple ways but didn't work
    void OnTriggerEnter(Collider pickUp)
    {
            pickUp.gameObject.SetActive(false);
            enemyCount = enemyCount + 10000; //this point for fun
            SetEnemyCountText();
    }

    // It works when the enemy hit the player
    void SetEnemyCountText()
    {
        enemyText.text = "Enemies Points: " + enemyCount.ToString();
    }
}
