﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    public Vector3 size;
    public GameObject player;

    Rigidbody rb;
    int count;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    // action when it hit an object
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            //action on the pickUp object
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        if (other.gameObject.CompareTag("enemy"))
        {
            //action on the enemy
            player.SetActive(false);
            winText.text = "You Lose!";
        }
    }

    //change the count 
    void SetCountText ()
    {
        countText.text = "Your Points: " + count.ToString();
        // win when player eat 35 balls
        if (count >= 35)
        {
            winText.text = "You Win!";
        }
    }
}
